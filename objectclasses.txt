# extended LDIF
#
# LDAPv3
# base <cn=subschema> with scope baseObject
# filter: (objectclass=*)
# requesting: objectclasses 
#

# Subschema
dn: cn=Subschema
objectClasses: ( 2.5.6.0 NAME 'top' DESC 'top of the superclass chain' ABSTRAC
 T MUST objectClass )
objectClasses: ( 1.3.6.1.4.1.1466.101.120.111 NAME 'extensibleObject' DESC 'RF
 C4512: extensible object' SUP top AUXILIARY )
objectClasses: ( 2.5.6.1 NAME 'alias' DESC 'RFC4512: an alias' SUP top STRUCTU
 RAL MUST aliasedObjectName )
objectClasses: ( 2.16.840.1.113730.3.2.6 NAME 'referral' DESC 'namedref: named
  subordinate referral' SUP top STRUCTURAL MUST ref )
objectClasses: ( 1.3.6.1.4.1.4203.1.4.1 NAME ( 'OpenLDAProotDSE' 'LDAProotDSE'
  ) DESC 'OpenLDAP Root DSE object' SUP top STRUCTURAL MAY cn )
objectClasses: ( 2.5.17.0 NAME 'subentry' DESC 'RFC3672: subentry' SUP top STR
 UCTURAL MUST ( cn $ subtreeSpecification ) )
objectClasses: ( 2.5.20.1 NAME 'subschema' DESC 'RFC4512: controlling subschem
 a (sub)entry' AUXILIARY MAY ( dITStructureRules $ nameForms $ dITContentRules
  $ objectClasses $ attributeTypes $ matchingRules $ matchingRuleUse ) )
objectClasses: ( 1.3.6.1.4.1.1466.101.119.2 NAME 'dynamicObject' DESC 'RFC2589
 : Dynamic Object' SUP top AUXILIARY )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.0.0 NAME 'olcConfig' DESC 'OpenLDAP
  configuration object' SUP top ABSTRACT )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.0.1 NAME 'olcGlobal' DESC 'OpenLDAP
  Global configuration options' SUP olcConfig STRUCTURAL MAY ( cn $ olcConfigF
 ile $ olcConfigDir $ olcAllows $ olcArgsFile $ olcAttributeOptions $ olcAuthI
 DRewrite $ olcAuthzPolicy $ olcAuthzRegexp $ olcConcurrency $ olcConnMaxPendi
 ng $ olcConnMaxPendingAuth $ olcDisallows $ olcGentleHUP $ olcIdleTimeout $ o
 lcIndexSubstrIfMaxLen $ olcIndexSubstrIfMinLen $ olcIndexSubstrAnyLen $ olcIn
 dexSubstrAnyStep $ olcIndexIntLen $ olcListenerThreads $ olcLocalSSF $ olcLog
 File $ olcLogLevel $ olcPasswordCryptSaltFormat $ olcPasswordHash $ olcPidFil
 e $ olcPluginLogFile $ olcReadOnly $ olcReferral $ olcReplogFile $ olcRequire
 s $ olcRestrict $ olcReverseLookup $ olcRootDSE $ olcSaslAuxprops $ olcSaslHo
 st $ olcSaslRealm $ olcSaslSecProps $ olcSecurity $ olcServerID $ olcSizeLimi
 t $ olcSockbufMaxIncoming $ olcSockbufMaxIncomingAuth $ olcTCPBuffer $ olcThr
 eads $ olcTimeLimit $ olcTLSCACertificateFile $ olcTLSCACertificatePath $ olc
 TLSCertificateFile $ olcTLSCertificateKeyFile $ olcTLSCipherSuite $ olcTLSCRL
 Check $ olcTLSRandFile $ olcTLSVerifyClient $ olcTLSDHParamFile $ olcTLSECNam
 e $ olcTLSCRLFile $ olcTLSProtocolMin $ olcToolThreads $ olcWriteTimeout $ ol
 cObjectIdentifier $ olcAttributeTypes $ olcObjectClasses $ olcDitContentRules
  $ olcLdapSyntaxes ) )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.0.2 NAME 'olcSchemaConfig' DESC 'Op
 enLDAP schema object' SUP olcConfig STRUCTURAL MAY ( cn $ olcObjectIdentifier
  $ olcLdapSyntaxes $ olcAttributeTypes $ olcObjectClasses $ olcDitContentRule
 s ) )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.0.3 NAME 'olcBackendConfig' DESC 'O
 penLDAP Backend-specific options' SUP olcConfig STRUCTURAL MUST olcBackend )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.0.4 NAME 'olcDatabaseConfig' DESC '
 OpenLDAP Database-specific options' SUP olcConfig STRUCTURAL MUST olcDatabase
  MAY ( olcHidden $ olcSuffix $ olcSubordinate $ olcAccess $ olcAddContentAcl 
 $ olcLastMod $ olcLimits $ olcMaxDerefDepth $ olcPlugin $ olcReadOnly $ olcRe
 plica $ olcReplicaArgsFile $ olcReplicaPidFile $ olcReplicationInterval $ olc
 ReplogFile $ olcRequires $ olcRestrict $ olcRootDN $ olcRootPW $ olcSchemaDN 
 $ olcSecurity $ olcSizeLimit $ olcSyncUseSubentry $ olcSyncrepl $ olcTimeLimi
 t $ olcUpdateDN $ olcUpdateRef $ olcMirrorMode $ olcMonitoring $ olcExtraAttr
 s ) )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.0.5 NAME 'olcOverlayConfig' DESC 'O
 penLDAP Overlay-specific options' SUP olcConfig STRUCTURAL MUST olcOverlay )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.0.6 NAME 'olcIncludeFile' DESC 'Ope
 nLDAP configuration include file' SUP olcConfig STRUCTURAL MUST olcInclude MA
 Y ( cn $ olcRootDSE ) )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.0.7 NAME 'olcFrontendConfig' DESC '
 OpenLDAP frontend configuration' AUXILIARY MAY ( olcDefaultSearchBase $ olcPa
 sswordHash $ olcSortVals ) )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.0.8 NAME 'olcModuleList' DESC 'Open
 LDAP dynamic module info' SUP olcConfig STRUCTURAL MAY ( cn $ olcModulePath $
  olcModuleLoad ) )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.2.2.1 NAME 'olcLdifConfig' DESC 'LD
 IF backend configuration' SUP olcDatabaseConfig STRUCTURAL MUST olcDbDirector
 y )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.2.12.1 NAME 'olcMdbConfig' DESC 'MD
 B backend configuration' SUP olcDatabaseConfig STRUCTURAL MUST olcDbDirectory
  MAY ( olcDbCheckpoint $ olcDbEnvFlags $ olcDbNoSync $ olcDbIndex $ olcDbMaxR
 eaders $ olcDbMaxSize $ olcDbMode $ olcDbSearchStack $ olcDbRtxnSize ) )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.3.18.1 NAME 'olcMemberOf' DESC 'Mem
 ber-of configuration' SUP olcOverlayConfig STRUCTURAL MAY ( olcMemberOfDN $ o
 lcMemberOfDangling $ olcMemberOfDanglingError $ olcMemberOfRefInt $ olcMember
 OfGroupOC $ olcMemberOfMemberAD $ olcMemberOfMemberOfAD ) )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.3.11.1 NAME 'olcRefintConfig' DESC 
 'Referential integrity configuration' SUP olcOverlayConfig STRUCTURAL MAY ( o
 lcRefintAttribute $ olcRefintNothing $ olcRefintModifiersName ) )
objectClasses: ( 1.3.6.1.4.1.4203.1.12.2.4.3.12.1 NAME 'olcPPolicyConfig' DESC
  'Password Policy configuration' SUP olcOverlayConfig STRUCTURAL MAY ( olcPPo
 licyDefault $ olcPPolicyHashCleartext $ olcPPolicyUseLockout $ olcPPolicyForw
 ardUpdates ) )
objectClasses: ( 2.5.6.2 NAME 'country' DESC 'RFC2256: a country' SUP top STRU
 CTURAL MUST c MAY ( searchGuide $ description ) )
objectClasses: ( 2.5.6.3 NAME 'locality' DESC 'RFC2256: a locality' SUP top ST
 RUCTURAL MAY ( street $ seeAlso $ searchGuide $ st $ l $ description ) )
objectClasses: ( 2.5.6.4 NAME 'organization' DESC 'RFC2256: an organization' S
 UP top STRUCTURAL MUST o MAY ( userPassword $ searchGuide $ seeAlso $ busines
 sCategory $ x121Address $ registeredAddress $ destinationIndicator $ preferre
 dDeliveryMethod $ telexNumber $ teletexTerminalIdentifier $ telephoneNumber $
  internationaliSDNNumber $ facsimileTelephoneNumber $ street $ postOfficeBox 
 $ postalCode $ postalAddress $ physicalDeliveryOfficeName $ st $ l $ descript
 ion ) )
objectClasses: ( 2.5.6.5 NAME 'organizationalUnit' DESC 'RFC2256: an organizat
 ional unit' SUP top STRUCTURAL MUST ou MAY ( userPassword $ searchGuide $ see
 Also $ businessCategory $ x121Address $ registeredAddress $ destinationIndica
 tor $ preferredDeliveryMethod $ telexNumber $ teletexTerminalIdentifier $ tel
 ephoneNumber $ internationaliSDNNumber $ facsimileTelephoneNumber $ street $ 
 postOfficeBox $ postalCode $ postalAddress $ physicalDeliveryOfficeName $ st 
 $ l $ description ) )
objectClasses: ( 2.5.6.6 NAME 'person' DESC 'RFC2256: a person' SUP top STRUCT
 URAL MUST ( sn $ cn ) MAY ( userPassword $ telephoneNumber $ seeAlso $ descri
 ption ) )
objectClasses: ( 2.5.6.7 NAME 'organizationalPerson' DESC 'RFC2256: an organiz
 ational person' SUP person STRUCTURAL MAY ( title $ x121Address $ registeredA
 ddress $ destinationIndicator $ preferredDeliveryMethod $ telexNumber $ telet
 exTerminalIdentifier $ telephoneNumber $ internationaliSDNNumber $ facsimileT
 elephoneNumber $ street $ postOfficeBox $ postalCode $ postalAddress $ physic
 alDeliveryOfficeName $ ou $ st $ l ) )
objectClasses: ( 2.5.6.8 NAME 'organizationalRole' DESC 'RFC2256: an organizat
 ional role' SUP top STRUCTURAL MUST cn MAY ( x121Address $ registeredAddress 
 $ destinationIndicator $ preferredDeliveryMethod $ telexNumber $ teletexTermi
 nalIdentifier $ telephoneNumber $ internationaliSDNNumber $ facsimileTelephon
 eNumber $ seeAlso $ roleOccupant $ preferredDeliveryMethod $ street $ postOff
 iceBox $ postalCode $ postalAddress $ physicalDeliveryOfficeName $ ou $ st $ 
 l $ description ) )
objectClasses: ( 2.5.6.9 NAME 'groupOfNames' DESC 'RFC2256: a group of names (
 DNs)' SUP top STRUCTURAL MUST ( member $ cn ) MAY ( businessCategory $ seeAls
 o $ owner $ ou $ o $ description ) )
objectClasses: ( 2.5.6.10 NAME 'residentialPerson' DESC 'RFC2256: an residenti
 al person' SUP person STRUCTURAL MUST l MAY ( businessCategory $ x121Address 
 $ registeredAddress $ destinationIndicator $ preferredDeliveryMethod $ telexN
 umber $ teletexTerminalIdentifier $ telephoneNumber $ internationaliSDNNumber
  $ facsimileTelephoneNumber $ preferredDeliveryMethod $ street $ postOfficeBo
 x $ postalCode $ postalAddress $ physicalDeliveryOfficeName $ st $ l ) )
objectClasses: ( 2.5.6.11 NAME 'applicationProcess' DESC 'RFC2256: an applicat
 ion process' SUP top STRUCTURAL MUST cn MAY ( seeAlso $ ou $ l $ description 
 ) )
objectClasses: ( 2.5.6.12 NAME 'applicationEntity' DESC 'RFC2256: an applicati
 on entity' SUP top STRUCTURAL MUST ( presentationAddress $ cn ) MAY ( support
 edApplicationContext $ seeAlso $ ou $ o $ l $ description ) )
objectClasses: ( 2.5.6.13 NAME 'dSA' DESC 'RFC2256: a directory system agent (
 a server)' SUP applicationEntity STRUCTURAL MAY knowledgeInformation )
objectClasses: ( 2.5.6.14 NAME 'device' DESC 'RFC2256: a device' SUP top STRUC
 TURAL MUST cn MAY ( serialNumber $ seeAlso $ owner $ ou $ o $ l $ description
  ) )
objectClasses: ( 2.5.6.15 NAME 'strongAuthenticationUser' DESC 'RFC2256: a str
 ong authentication user' SUP top AUXILIARY MUST userCertificate )
objectClasses: ( 2.5.6.16 NAME 'certificationAuthority' DESC 'RFC2256: a certi
 ficate authority' SUP top AUXILIARY MUST ( authorityRevocationList $ certific
 ateRevocationList $ cACertificate ) MAY crossCertificatePair )
objectClasses: ( 2.5.6.17 NAME 'groupOfUniqueNames' DESC 'RFC2256: a group of 
 unique names (DN and Unique Identifier)' SUP top STRUCTURAL MUST ( uniqueMemb
 er $ cn ) MAY ( businessCategory $ seeAlso $ owner $ ou $ o $ description ) )
objectClasses: ( 2.5.6.18 NAME 'userSecurityInformation' DESC 'RFC2256: a user
  security information' SUP top AUXILIARY MAY supportedAlgorithms )
objectClasses: ( 2.5.6.16.2 NAME 'certificationAuthority-V2' SUP certification
 Authority AUXILIARY MAY deltaRevocationList )
objectClasses: ( 2.5.6.19 NAME 'cRLDistributionPoint' SUP top STRUCTURAL MUST 
 cn MAY ( certificateRevocationList $ authorityRevocationList $ deltaRevocatio
 nList ) )
objectClasses: ( 2.5.6.20 NAME 'dmd' SUP top STRUCTURAL MUST dmdName MAY ( use
 rPassword $ searchGuide $ seeAlso $ businessCategory $ x121Address $ register
 edAddress $ destinationIndicator $ preferredDeliveryMethod $ telexNumber $ te
 letexTerminalIdentifier $ telephoneNumber $ internationaliSDNNumber $ facsimi
 leTelephoneNumber $ street $ postOfficeBox $ postalCode $ postalAddress $ phy
 sicalDeliveryOfficeName $ st $ l $ description ) )
objectClasses: ( 2.5.6.21 NAME 'pkiUser' DESC 'RFC2587: a PKI user' SUP top AU
 XILIARY MAY userCertificate )
objectClasses: ( 2.5.6.22 NAME 'pkiCA' DESC 'RFC2587: PKI certificate authorit
 y' SUP top AUXILIARY MAY ( authorityRevocationList $ certificateRevocationLis
 t $ cACertificate $ crossCertificatePair ) )
objectClasses: ( 2.5.6.23 NAME 'deltaCRL' DESC 'RFC2587: PKI user' SUP top AUX
 ILIARY MAY deltaRevocationList )
objectClasses: ( 1.3.6.1.4.1.250.3.15 NAME 'labeledURIObject' DESC 'RFC2079: o
 bject that contains the URI attribute type' SUP top AUXILIARY MAY labeledURI 
 )
objectClasses: ( 0.9.2342.19200300.100.4.19 NAME 'simpleSecurityObject' DESC '
 RFC1274: simple security object' SUP top AUXILIARY MUST userPassword )
objectClasses: ( 1.3.6.1.4.1.1466.344 NAME 'dcObject' DESC 'RFC2247: domain co
 mponent object' SUP top AUXILIARY MUST dc )
objectClasses: ( 1.3.6.1.1.3.1 NAME 'uidObject' DESC 'RFC2377: uid object' SUP
  top AUXILIARY MUST uid )
objectClasses: ( 0.9.2342.19200300.100.4.4 NAME ( 'pilotPerson' 'newPilotPerso
 n' ) SUP person STRUCTURAL MAY ( userid $ textEncodedORAddress $ rfc822Mailbo
 x $ favouriteDrink $ roomNumber $ userClass $ homeTelephoneNumber $ homePosta
 lAddress $ secretary $ personalTitle $ preferredDeliveryMethod $ businessCate
 gory $ janetMailbox $ otherMailbox $ mobileTelephoneNumber $ pagerTelephoneNu
 mber $ organizationalStatus $ mailPreferenceOption $ personalSignature ) )
objectClasses: ( 0.9.2342.19200300.100.4.5 NAME 'account' SUP top STRUCTURAL M
 UST userid MAY ( description $ seeAlso $ localityName $ organizationName $ or
 ganizationalUnitName $ host ) )
objectClasses: ( 0.9.2342.19200300.100.4.6 NAME 'document' SUP top STRUCTURAL 
 MUST documentIdentifier MAY ( commonName $ description $ seeAlso $ localityNa
 me $ organizationName $ organizationalUnitName $ documentTitle $ documentVers
 ion $ documentAuthor $ documentLocation $ documentPublisher ) )
objectClasses: ( 0.9.2342.19200300.100.4.7 NAME 'room' SUP top STRUCTURAL MUST
  commonName MAY ( roomNumber $ description $ seeAlso $ telephoneNumber ) )
objectClasses: ( 0.9.2342.19200300.100.4.9 NAME 'documentSeries' SUP top STRUC
 TURAL MUST commonName MAY ( description $ seeAlso $ telephonenumber $ localit
 yName $ organizationName $ organizationalUnitName ) )
objectClasses: ( 0.9.2342.19200300.100.4.13 NAME 'domain' SUP top STRUCTURAL M
 UST domainComponent MAY ( associatedName $ organizationName $ description $ b
 usinessCategory $ seeAlso $ searchGuide $ userPassword $ localityName $ state
 OrProvinceName $ streetAddress $ physicalDeliveryOfficeName $ postalAddress $
  postalCode $ postOfficeBox $ streetAddress $ facsimileTelephoneNumber $ inte
 rnationalISDNNumber $ telephoneNumber $ teletexTerminalIdentifier $ telexNumb
 er $ preferredDeliveryMethod $ destinationIndicator $ registeredAddress $ x12
 1Address ) )
objectClasses: ( 0.9.2342.19200300.100.4.14 NAME 'RFC822localPart' SUP domain 
 STRUCTURAL MAY ( commonName $ surname $ description $ seeAlso $ telephoneNumb
 er $ physicalDeliveryOfficeName $ postalAddress $ postalCode $ postOfficeBox 
 $ streetAddress $ facsimileTelephoneNumber $ internationalISDNNumber $ teleph
 oneNumber $ teletexTerminalIdentifier $ telexNumber $ preferredDeliveryMethod
  $ destinationIndicator $ registeredAddress $ x121Address ) )
objectClasses: ( 0.9.2342.19200300.100.4.15 NAME 'dNSDomain' SUP domain STRUCT
 URAL MAY ( ARecord $ MDRecord $ MXRecord $ NSRecord $ SOARecord $ CNAMERecord
  ) )
objectClasses: ( 0.9.2342.19200300.100.4.17 NAME 'domainRelatedObject' DESC 'R
 FC1274: an object related to an domain' SUP top AUXILIARY MUST associatedDoma
 in )
objectClasses: ( 0.9.2342.19200300.100.4.18 NAME 'friendlyCountry' SUP country
  STRUCTURAL MUST friendlyCountryName )
objectClasses: ( 0.9.2342.19200300.100.4.20 NAME 'pilotOrganization' SUP ( org
 anization $ organizationalUnit ) STRUCTURAL MAY buildingName )
objectClasses: ( 0.9.2342.19200300.100.4.21 NAME 'pilotDSA' SUP dsa STRUCTURAL
  MAY dSAQuality )
objectClasses: ( 0.9.2342.19200300.100.4.22 NAME 'qualityLabelledData' SUP top
  AUXILIARY MUST dsaQuality MAY ( subtreeMinimumQuality $ subtreeMaximumQualit
 y ) )
objectClasses: ( 1.3.6.1.1.1.2.0 NAME 'posixAccount' DESC 'Abstraction of an a
 ccount with POSIX attributes' SUP top AUXILIARY MUST ( cn $ uid $ uidNumber $
  gidNumber $ homeDirectory ) MAY ( userPassword $ loginShell $ gecos $ descri
 ption ) )
objectClasses: ( 1.3.6.1.1.1.2.1 NAME 'shadowAccount' DESC 'Additional attribu
 tes for shadow passwords' SUP top AUXILIARY MUST uid MAY ( userPassword $ sha
 dowLastChange $ shadowMin $ shadowMax $ shadowWarning $ shadowInactive $ shad
 owExpire $ shadowFlag $ description ) )
objectClasses: ( 1.3.6.1.1.1.2.2 NAME 'posixGroup' DESC 'Abstraction of a grou
 p of accounts' SUP top STRUCTURAL MUST ( cn $ gidNumber ) MAY ( userPassword 
 $ memberUid $ description ) )
objectClasses: ( 1.3.6.1.1.1.2.3 NAME 'ipService' DESC 'Abstraction an Interne
 t Protocol service' SUP top STRUCTURAL MUST ( cn $ ipServicePort $ ipServiceP
 rotocol ) MAY description )
objectClasses: ( 1.3.6.1.1.1.2.4 NAME 'ipProtocol' DESC 'Abstraction of an IP 
 protocol' SUP top STRUCTURAL MUST ( cn $ ipProtocolNumber $ description ) MAY
  description )
objectClasses: ( 1.3.6.1.1.1.2.5 NAME 'oncRpc' DESC 'Abstraction of an ONC/RPC
  binding' SUP top STRUCTURAL MUST ( cn $ oncRpcNumber $ description ) MAY des
 cription )
objectClasses: ( 1.3.6.1.1.1.2.6 NAME 'ipHost' DESC 'Abstraction of a host, an
  IP device' SUP top AUXILIARY MUST ( cn $ ipHostNumber ) MAY ( l $ descriptio
 n $ manager ) )
objectClasses: ( 1.3.6.1.1.1.2.7 NAME 'ipNetwork' DESC 'Abstraction of an IP n
 etwork' SUP top STRUCTURAL MUST ( cn $ ipNetworkNumber ) MAY ( ipNetmaskNumbe
 r $ l $ description $ manager ) )
objectClasses: ( 1.3.6.1.1.1.2.8 NAME 'nisNetgroup' DESC 'Abstraction of a net
 group' SUP top STRUCTURAL MUST cn MAY ( nisNetgroupTriple $ memberNisNetgroup
  $ description ) )
objectClasses: ( 1.3.6.1.1.1.2.9 NAME 'nisMap' DESC 'A generic abstraction of 
 a NIS map' SUP top STRUCTURAL MUST nisMapName MAY description )
objectClasses: ( 1.3.6.1.1.1.2.10 NAME 'nisObject' DESC 'An entry in a NIS map
 ' SUP top STRUCTURAL MUST ( cn $ nisMapEntry $ nisMapName ) MAY description )
objectClasses: ( 1.3.6.1.1.1.2.11 NAME 'ieee802Device' DESC 'A device with a M
 AC address' SUP top AUXILIARY MAY macAddress )
objectClasses: ( 1.3.6.1.1.1.2.12 NAME 'bootableDevice' DESC 'A device with bo
 ot parameters' SUP top AUXILIARY MAY ( bootFile $ bootParameter ) )
objectClasses: ( 2.16.840.1.113730.3.2.2 NAME 'inetOrgPerson' DESC 'RFC2798: I
 nternet Organizational Person' SUP organizationalPerson STRUCTURAL MAY ( audi
 o $ businessCategory $ carLicense $ departmentNumber $ displayName $ employee
 Number $ employeeType $ givenName $ homePhone $ homePostalAddress $ initials 
 $ jpegPhoto $ labeledURI $ mail $ manager $ mobile $ o $ pager $ photo $ room
 Number $ secretary $ uid $ userCertificate $ x500uniqueIdentifier $ preferred
 Language $ userSMIMECertificate $ userPKCS12 ) )
objectClasses: ( 1.3.6.1.4.1.4754.2.99.1 NAME 'pwdPolicyChecker' SUP top AUXIL
 IARY MAY pwdCheckModule )
objectClasses: ( 1.3.6.1.4.1.42.2.27.8.2.1 NAME 'pwdPolicy' SUP top AUXILIARY 
 MUST pwdAttribute MAY ( pwdMinAge $ pwdMaxAge $ pwdInHistory $ pwdCheckQualit
 y $ pwdMinLength $ pwdExpireWarning $ pwdGraceAuthNLimit $ pwdLockout $ pwdLo
 ckoutDuration $ pwdMaxFailure $ pwdFailureCountInterval $ pwdMustChange $ pwd
 AllowUserChange $ pwdSafeModify $ pwdMaxRecordedFailure ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.1 NAME 'dhcpService' DESC 'Service 
 object that represents the actual DHCP Service configuration. This is a conta
 iner object.' SUP top STRUCTURAL MUST cn MAY ( dhcpPrimaryDN $ dhcpSecondaryD
 N $ dhcpServerDN $ dhcpSharedNetworkDN $ dhcpSubnetDN $ dhcpGroupDN $ dhcpHos
 tDN $ dhcpClassesDN $ dhcpOptionsDN $ dhcpZoneDN $ dhcpKeyDN $ dhcpFailOverPe
 erDN $ dhcpStatements $ dhcpComments $ dhcpOption ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.2 NAME 'dhcpSharedNetwork' DESC 'Th
 is stores configuration information for a shared network.' SUP top STRUCTURAL
  MUST cn MAY ( dhcpSubnetDN $ dhcpPoolDN $ dhcpOptionsDN $ dhcpZoneDN $ dhcpS
 tatements $ dhcpComments $ dhcpOption ) X-NDS_CONTAINMENT 'dhcpService' )
objectClasses: ( 2.16.840.1.113719.1.203.6.3 NAME 'dhcpSubnet' DESC 'This clas
 s defines a subnet. This is a container object.' SUP top STRUCTURAL MUST ( cn
  $ dhcpNetMask ) MAY ( dhcpRange $ dhcpPoolDN $ dhcpGroupDN $ dhcpHostDN $ dh
 cpClassesDN $ dhcpLeasesDN $ dhcpOptionsDN $ dhcpZoneDN $ dhcpKeyDN $ dhcpFai
 lOverPeerDN $ dhcpStatements $ dhcpComments $ dhcpOption ) X-NDS_CONTAINMENT 
 ( 'dhcpService' 'dhcpSharedNetwork' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.4 NAME 'dhcpPool' DESC 'This stores
  configuration information about a pool.' SUP top STRUCTURAL MUST ( cn $ dhcp
 Range ) MAY ( dhcpClassesDN $ dhcpPermitList $ dhcpLeasesDN $ dhcpOptionsDN $
  dhcpZoneDN $ dhcpKeyDN $ dhcpStatements $ dhcpComments $ dhcpOption ) X-NDS_
 CONTAINMENT ( 'dhcpSubnet' 'dhcpSharedNetwork' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.5 NAME 'dhcpGroup' DESC 'Group obje
 ct that lists host DNs and parameters. This is a container object.' SUP top S
 TRUCTURAL MUST cn MAY ( dhcpHostDN $ dhcpOptionsDN $ dhcpStatements $ dhcpCom
 ments $ dhcpOption ) X-NDS_CONTAINMENT ( 'dhcpSubnet' 'dhcpService' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.6 NAME 'dhcpHost' DESC 'This repres
 ents information about a particular client' SUP top STRUCTURAL MUST cn MAY ( 
 dhcpLeaseDN $ dhcpHWAddress $ dhcpOptionsDN $ dhcpStatements $ dhcpComments $
  dhcpOption ) X-NDS_CONTAINMENT ( 'dhcpService' 'dhcpSubnet' 'dhcpGroup' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.7 NAME 'dhcpClass' DESC 'Represents
  information about a collection of related clients.' SUP top STRUCTURAL MUST 
 cn MAY ( dhcpSubClassesDN $ dhcpOptionsDN $ dhcpStatements $ dhcpComments $ d
 hcpOption ) X-NDS_CONTAINMENT ( 'dhcpService' 'dhcpSubnet' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.8 NAME 'dhcpSubClass' DESC 'Represe
 nts information about a collection of related classes.' SUP top STRUCTURAL MU
 ST cn MAY ( dhcpClassData $ dhcpOptionsDN $ dhcpStatements $ dhcpComments $ d
 hcpOption ) X-NDS_CONTAINMENT 'dhcpClass' )
objectClasses: ( 2.16.840.1.113719.1.203.6.9 NAME 'dhcpOptions' DESC 'Represen
 ts information about a collection of options defined.' SUP top AUXILIARY MUST
  cn MAY ( dhcpOption $ dhcpComments ) X-NDS_CONTAINMENT ( 'dhcpService' 'dhcp
 SharedNetwork' 'dhcpSubnet' 'dhcpPool' 'dhcpGroup' 'dhcpHost' 'dhcpClass' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.10 NAME 'dhcpLeases' DESC 'This cla
 ss represents an IP Address, which may or may not have been leased.' SUP top 
 STRUCTURAL MUST ( cn $ dhcpAddressState ) MAY ( dhcpExpirationTime $ dhcpStar
 tTimeOfState $ dhcpLastTransactionTime $ dhcpBootpFlag $ dhcpDomainName $ dhc
 pDnsStatus $ dhcpRequestedHostName $ dhcpAssignedHostName $ dhcpReservedForCl
 ient $ dhcpAssignedToClient $ dhcpRelayAgentInfo $ dhcpHWAddress $ dhcpOption
  ) X-NDS_CONTAINMENT ( 'dhcpService' 'dhcpSubnet' 'dhcpPool' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.11 NAME 'dhcpLog' DESC 'This is the
  object that holds past information about the IP address. The cn is the time/
 date stamp when the address was assigned or released, the address state at th
 e time, if the address was assigned or released.' SUP top STRUCTURAL MUST cn 
 MAY ( dhcpAddressState $ dhcpExpirationTime $ dhcpStartTimeOfState $ dhcpLast
 TransactionTime $ dhcpBootpFlag $ dhcpDomainName $ dhcpDnsStatus $ dhcpReques
 tedHostName $ dhcpAssignedHostName $ dhcpReservedForClient $ dhcpAssignedToCl
 ient $ dhcpRelayAgentInfo $ dhcpHWAddress $ dhcpErrorLog ) X-NDS_CONTAINMENT 
 ( 'dhcpLeases' 'dhcpPool' 'dhcpSubnet' 'dhcpSharedNetwork' 'dhcpService' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.12 NAME 'dhcpServer' DESC 'DHCP Ser
 ver Object' SUP top STRUCTURAL MUST cn MAY ( dhcpServiceDN $ dhcpLocatorDN $ 
 dhcpVersion $ dhcpImplementation $ dhcpHashBucketAssignment $ dhcpDelayedServ
 iceParameter $ dhcpMaxClientLeadTime $ dhcpFailOverEndpointState $ dhcpStatem
 ents $ dhcpComments $ dhcpOption ) X-NDS_CONTAINMENT ( 'organization' 'organi
 zationalunit' 'domain' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.13 NAME 'dhcpTSigKey' DESC 'TSIG ke
 y for secure dynamic updates' SUP top STRUCTURAL MUST ( cn $ dhcpKeyAlgorithm
  $ dhcpKeySecret ) MAY dhcpComments X-NDS_CONTAINMENT ( 'dhcpService' 'dhcpSh
 aredNetwork' 'dhcpSubnet' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.14 NAME 'dhcpDnsZone' DESC 'DNS Zon
 e for updating leases' SUP top STRUCTURAL MUST ( cn $ dhcpDnsZoneServer ) MAY
  ( dhcpKeyDN $ dhcpComments ) X-NDS_CONTAINMENT ( 'dhcpService' 'dhcpSharedNe
 twork' 'dhcpSubnet' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.15 NAME 'dhcpFailOverPeer' DESC 'Th
 is class defines the Fail over peer' SUP top STRUCTURAL MUST ( cn $ dhcpFailO
 verRole $ dhcpFailOverReceiveAddress $ dhcpFailOverPeerAddress $ dhcpFailover
 ReceivePort $ dhcpFailOverPeerPort ) MAY ( dhcpFailOverResponseDelay $ dhcpFa
 ilOverUnpackedUpdates $ dhcpMaxClientLeadTime $ dhcpFailOverSplit $ dhcpHashB
 ucketAssignment $ dhcpFailOverLoadBalanceTime $ dhcpComments $ dhcpOption ) X
 -NDS_CONTAINMENT ( 'dhcpService' 'dhcpSharedNetwork' 'dhcpSubnet' ) )
objectClasses: ( 2.16.840.1.113719.1.203.6.16 NAME 'dhcpLocator' DESC 'Locator
  object for DHCP configuration in the tree. There will be a single dhcpLocato
 r object in the tree with links to all the DHCP objects in the tree' SUP top 
 STRUCTURAL MUST cn MAY ( dhcpServiceDN $ dhcpServerDN $ dhcpSharedNetworkDN $
  dhcpSubnetDN $ dhcpPoolDN $ dhcpGroupDN $ dhcpHostDN $ dhcpClassesDN $ dhcpK
 eyDN $ dhcpZoneDN $ dhcpFailOverPeerDN $ dhcpOption $ dhcpComments ) X-NDS_CO
 NTAINMENT ( 'organization' 'organizationalunit' 'domain' ) )
objectClasses: ( 1.3.6.1.4.1.2428.20.3 NAME 'dNSZone' SUP top STRUCTURAL MUST 
 ( zoneName $ relativeDomainName ) MAY ( DNSTTL $ DNSClass $ ARecord $ MDRecor
 d $ MXRecord $ NSRecord $ SOARecord $ CNAMERecord $ PTRRecord $ HINFORecord $
  MINFORecord $ TXTRecord $ AFSDBRecord $ SIGRecord $ KEYRecord $ AAAARecord $
  LOCRecord $ NXTRecord $ SRVRecord $ NAPTRRecord $ KXRecord $ CERTRecord $ A6
 Record $ DNAMERecord $ DSRecord $ SSHFPRecord $ RRSIGRecord $ NSECRecord ) )
objectClasses: ( 1.3.6.1.4.1.21103.1.2.13.1 NAME 'mailAccount' DESC 'Mail Acco
 unt' SUP top AUXILIARY MUST mail MAY ( mailalias $ maildrop $ mailenable $ ma
 ilbox $ mailuserquota $ mailhost $ mailproxy $ mailhidden ) )
objectClasses: ( 1.3.6.1.4.1.21103.1.2.13.2 NAME 'mailDomain' DESC 'Domain mai
 l entry' SUP top STRUCTURAL MUST virtualdomain MAY ( virtualdomaindescription
  $ mailuserquota ) )
objectClasses: ( 1.3.6.1.4.1.21103.1.2.13.3 NAME 'mailGroup' DESC 'Mail Group'
  SUP top AUXILIARY MUST mail MAY mailhidden )
objectClasses: ( 1.3.6.1.4.1.21103.1.2.13.4 NAME 'mailAlias' DESC 'Mail Alias'
  SUP top STRUCTURAL MUST mailalias MAY ( mail $ mailaliasmember $ mailenable 
 ) )
objectClasses: ( 1.3.6.1.4.1.40098.1.2.1.19.1 NAME 'lmcUserObject' DESC 'Objec
 tclass for LMC user settings ' AUXILIARY MAY ( lmcACL $ lmcPrefMode $ lmcPrin
 terAllowed ) )
objectClasses: ( 1.3.6.1.4.1.24552.500.1.1.2.0 NAME 'ldapPublicKey' DESC 'MAND
 ATORY: OpenSSH LPK objectclass' SUP top AUXILIARY MAY ( sshPublicKey $ uid ) 
 )
objectClasses: ( 1.3.6.1.4.1.19937.1.2.1 NAME 'systemQuotas' DESC 'System Quot
 as' SUP posixAccount AUXILIARY MUST uid MAY ( quota $ networkquota ) )
objectClasses: ( 1.3.6.1.4.1.19937.1.2.2 NAME 'defaultQuotas' DESC 'Quota defa
 ults to apply to members of a group' SUP top AUXILIARY MUST cn MAY ( quota $ 
 networkquota ) )
objectClasses: ( 1.3.6.1.4.1.3317.4.3.2.1 NAME 'radiusprofile' DESC '' SUP top
  AUXILIARY MUST cn MAY ( radiusArapFeatures $ radiusArapSecurity $ radiusArap
 ZoneAccess $ radiusAuthType $ radiusCallbackId $ radiusCallbackNumber $ radiu
 sCalledStationId $ radiusCallingStationId $ radiusClass $ radiusClientIPAddre
 ss $ radiusFilterId $ radiusFramedAppleTalkLink $ radiusFramedAppleTalkNetwor
 k $ radiusFramedAppleTalkZone $ radiusFramedCompression $ radiusFramedIPAddre
 ss $ radiusFramedIPNetmask $ radiusFramedIPXNetwork $ radiusFramedMTU $ radiu
 sFramedProtocol $ radiusCheckItem $ radiusReplyItem $ radiusFramedRoute $ rad
 iusFramedRouting $ radiusIdleTimeout $ radiusGroupName $ radiusHint $ radiusH
 untgroupName $ radiusLoginIPHost $ radiusLoginLATGroup $ radiusLoginLATNode $
  radiusLoginLATPort $ radiusLoginLATService $ radiusLoginService $ radiusLogi
 nTCPPort $ radiusLoginTime $ radiusPasswordRetry $ radiusPortLimit $ radiusPr
 ompt $ radiusProxyToRealm $ radiusRealm $ radiusReplicateToRealm $ radiusServ
 iceType $ radiusSessionTimeout $ radiusStripUserName $ radiusTerminationActio
 n $ radiusTunnelClientEndpoint $ radiusProfileDn $ radiusSimultaneousUse $ ra
 diusTunnelAssignmentId $ radiusTunnelMediumType $ radiusTunnelPassword $ radi
 usTunnelPreference $ radiusTunnelPrivateGroupId $ radiusTunnelServerEndpoint 
 $ radiusTunnelType $ radiusUserCategory $ radiusVSA $ radiusExpiration $ dial
 upAccess $ radiusNASIpAddress $ radiusReplyMessage ) )
objectClasses: ( 1.3.6.1.4.1.3317.4.3.2.2 NAME 'radiusObjectProfile' DESC 'A C
 ontainer Objectclass to be used for creating radius profile object' SUP top S
 TRUCTURAL MUST cn MAY ( uid $ userPassword $ description ) )
objectClasses: ( 1.3.6.1.4.1.7165.2.2.6 NAME 'sambaSamAccount' DESC 'Samba 3.0
  Auxilary SAM Account' SUP top AUXILIARY MUST ( uid $ sambaSID ) MAY ( cn $ s
 ambaLMPassword $ sambaNTPassword $ sambaPwdLastSet $ sambaLogonTime $ sambaLo
 goffTime $ sambaKickoffTime $ sambaPwdCanChange $ sambaPwdMustChange $ sambaA
 cctFlags $ displayName $ sambaHomePath $ sambaHomeDrive $ sambaLogonScript $ 
 sambaProfilePath $ description $ sambaUserWorkstations $ sambaPrimaryGroupSID
  $ sambaDomainName $ sambaMungedDial $ sambaBadPasswordCount $ sambaBadPasswo
 rdTime $ sambaPasswordHistory $ sambaLogonHours ) )
objectClasses: ( 1.3.6.1.4.1.7165.2.2.4 NAME 'sambaGroupMapping' DESC 'Samba G
 roup Mapping' SUP top AUXILIARY MUST ( gidNumber $ sambaSID $ sambaGroupType 
 ) MAY ( displayName $ description $ sambaSIDList ) )
objectClasses: ( 1.3.6.1.4.1.7165.2.2.14 NAME 'sambaTrustPassword' DESC 'Samba
  Trust Password' SUP top STRUCTURAL MUST ( sambaDomainName $ sambaNTPassword 
 $ sambaTrustFlags ) MAY ( sambaSID $ sambaPwdLastSet ) )
objectClasses: ( 1.3.6.1.4.1.7165.2.2.5 NAME 'sambaDomain' DESC 'Samba Domain 
 Information' SUP top STRUCTURAL MUST ( sambaDomainName $ sambaSID ) MAY ( sam
 baNextRid $ sambaNextGroupRid $ sambaNextUserRid $ sambaAlgorithmicRidBase $ 
 sambaMinPwdLength $ sambaPwdHistoryLength $ sambaLogonToChgPwd $ sambaMaxPwdA
 ge $ sambaMinPwdAge $ sambaLockoutDuration $ sambaLockoutObservationWindow $ 
 sambaLockoutThreshold $ sambaForceLogoff $ sambaRefuseMachinePwdChange ) )
objectClasses: ( 1.3.6.1.4.1.7165.2.2.7 NAME 'sambaUnixIdPool' DESC 'Pool for 
 allocating UNIX uids/gids' SUP top AUXILIARY MUST ( uidNumber $ gidNumber ) )
objectClasses: ( 1.3.6.1.4.1.7165.2.2.8 NAME 'sambaIdmapEntry' DESC 'Mapping f
 rom a SID to an ID' SUP top AUXILIARY MUST sambaSID MAY ( uidNumber $ gidNumb
 er ) )
objectClasses: ( 1.3.6.1.4.1.7165.2.2.9 NAME 'sambaSidEntry' DESC 'Structural 
 Class for a SID' SUP top STRUCTURAL MUST sambaSID )
objectClasses: ( 1.3.6.1.4.1.7165.2.2.10 NAME 'sambaConfig' DESC 'Samba Config
 uration Section' SUP top AUXILIARY MAY description )
objectClasses: ( 1.3.6.1.4.1.7165.2.2.11 NAME 'sambaShare' DESC 'Samba Share S
 ection' SUP top STRUCTURAL MUST sambaShareName MAY description )
objectClasses: ( 1.3.6.1.4.1.7165.2.2.12 NAME 'sambaConfigOption' DESC 'Samba 
 Configuration Option' SUP top STRUCTURAL MUST sambaOptionName MAY ( sambaBool
 Option $ sambaIntegerOption $ sambaStringOption $ sambaStringListoption $ des
 cription ) )
objectClasses: ( 1.3.6.1.4.1.26278.1.1.0.0 NAME 'zarafa-user' DESC 'Zarafa: an
  user of Zarafa' SUP top AUXILIARY MUST cn MAY ( zarafaQuotaOverride $ zarafa
 QuotaWarn $ zarafaQuotaSoft $ zarafaSendAsPrivilege $ zarafaQuotaHard $ zaraf
 aAdmin $ zarafaSharedStoreOnly $ zarafaResourceType $ zarafaResourceCapacity 
 $ zarafaAccount $ zarafaHidden $ zarafaAliases $ zarafaUserServer ) )
objectClasses: ( 1.3.6.1.4.1.26278.1.6.0.0 NAME 'zarafa-contact' DESC 'Zarafa:
  a contact of Zarafa' SUP top AUXILIARY MUST ( cn $ uidNumber ) MAY ( zarafaS
 endAsPrivilege $ zarafaHidden $ zarafaAliases ) )
objectClasses: ( 1.3.6.1.4.1.26278.1.2.0.0 NAME 'zarafa-group' DESC 'Zarafa: a
  group of Zarafa' SUP top AUXILIARY MUST cn MAY ( zarafaAccount $ zarafaHidde
 n $ mail $ zarafaAliases $ zarafaSecurityGroup ) )
objectClasses: ( 1.3.6.1.4.1.26278.1.3.0.0 NAME 'zarafa-company' DESC 'ZARAFA:
  a company of Zarafa' SUP top AUXILIARY MUST cn MAY ( zarafaAccount $ zarafaH
 idden $ zarafaViewPrivilege $ zarafaAdminPrivilege $ zarafaSystemAdmin $ zara
 faQuotaOverride $ zarafaQuotaWarn $ zarafaUserDefaultQuotaOverride $ zarafaUs
 erDefaultQuotaWarn $ zarafaUserDefaultQuotaSoft $ zarafaUserDefaultQuotaHard 
 $ zarafaQuotaUserWarningRecipients $ zarafaQuotaCompanyWarningRecipients $ za
 rafaCompanyServer ) )
objectClasses: ( 1.3.6.1.4.1.26278.1.4.0.0 NAME 'zarafa-server' DESC 'ZARAFA: 
 a Zarafa server' SUP top AUXILIARY MUST cn MAY ( zarafaAccount $ zarafaHidden
  $ zarafaHttpPort $ zarafaSslPort $ zarafaFilePath $ zarafaContainsPublic ) )
objectClasses: ( 1.3.6.1.4.1.26278.1.5.0.0 NAME 'zarafa-addresslist' DESC 'ZAR
 AFA: a Zarafa Addresslist' SUP top STRUCTURAL MUST cn MAY ( zarafaAccount $ z
 arafaHidden $ zarafaFilter $ zarafaBase ) )
objectClasses: ( 1.3.6.1.4.1.26278.1.7.0.0 NAME 'zarafa-dynamicgroup' DESC 'ZA
 RAFA: a Zarafa dynamic group' SUP top STRUCTURAL MUST cn MAY ( zarafaAccount 
 $ zarafaHidden $ mail $ zarafaAliases $ zarafaFilter $ zarafaBase ) )

# search result
search: 2
result: 0 Success

# numResponses: 2
# numEntries: 1
